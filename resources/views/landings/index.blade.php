@extends('app')
@section('title', $title)
@section('content')
    <div class="container">

        @include('layouts.nav')

        <a href="{{ URL::route('dashboard.landing.create') }}" class="btn btn-primary btn-block">Crear nuevo landing</a>

        <ul class="list-group">
            @forelse($landings as $landing)
                <a href="{{ URL::route('dashboard.landing.edit', $landing->id) }}" class="list-group-item">{{ $landing->title }}</a>
            @empty
            @endforelse
        </ul>

    </div>
@stop
