@extends('app')
@section('title', $title)
@section('content')
    <div class="container">

        @include('layouts.nav')

        @if($errors->any() or Session::has('errors'))
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4>Oh snap! You got an error!</h4>
                <ul>
                    @foreach ($errors->all(':message') as $message)
                        <li>{{ $message }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="POST" action="{{ URL::route('dashboard.landing.update', $landing->id) }}">

            <input type="hidden" name="_method" value="PUT"/>
            {{ csrf_field() }}

            <div class="form-group">
                <label for="name">Nombre (URL):</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ $landing->name }}"/>
            </div>

            <div class="form-group">
                <label for="title">Titulo: </label>
                <input type="text" id="title" name="title" class="form-control" value="{{ $landing->title }}"/>
            </div>

            <div class="form-group">
                <label for="view">Vista: </label>
                <input type="text" id="view" name="view" class="form-control" value="{{ $landing->view }}"/>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>

        </form>

    </div>
@stop
