@extends('app')
@section('content')

    <form method="POST" action="/auth/login" class="form-signin">
        {!! csrf_field() !!}
        <h2 class="form-signin-heading">Ingresar</h2>
        <div>
            <label for="email" class="sr-only">Correo Electrónico</label>
            <input type="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="Correo Electrónico" required autofocus>
        </div>

        <div>
            <label for="password" class="sr-only">Contraseña</label>
            <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox" value="remember"> Recuerdame en este computador
            </label>
        </div>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
    </form>
@stop
