<!DOCTYPE html>
<html lang="es-co">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-TileColor" content="#b2ba2c">
    <meta name="msapplication-TileImage" content="//latam.cdn.ulcommerce.com/ean/favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="//latam.cdn.ulcommerce.com/ean/favicons/browserconfig.xml">
    <meta name="theme-color" content="#b2ba2c">

    <title>Carreras Profesionales Presenciales - EAN  </title>
    <meta name="description" content="">
    <link rel="canonical" href="http://landing.ean.edu.co/carreras-profesionales-presenciales"/>
    <meta name="robots" content="index,follow">

    <link rel="apple-touch-icon" sizes="57x57" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="//latam.cdn.ulcommerce.com/ean/favicons/manifest.json">
    <link rel="shortcut icon" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon.ico">

    <!-- CSS -->
    <link href="//cdn.ulcommerce.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link href="//cdn.ulcommerce.com/WOW/1.0.2/css/animate.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header class="header">
    <div class="container">
        @include('layouts.notifications')
        <div class="row">
            <div class="col-md-2 col-sm-2 col-xs-4">
                <div class="logo">
                    <img src="/img/logo.png" class="center-block img-responsive">
                </div>
            </div>
            <div class="col-md-6 col-sm-6 hidden-xs">
                <img src="/img/text.png" class="text">
            </div>
            <div class="col-md-4 col-sm-4 col-xs-8 hidden-sm">
                <p class="text-center text2">
                    Línea gratuita nacional: <strong>01 8000 93 1000</strong> <br>
                    Centro de Contacto en Bogotá: <strong>593 6464</strong> <br>
                    <strong>Calle 79 No. 11 -45</strong> - Bogotá, Colombia</p>
            </div>
        </div>
    </div>
</header>

<div class="tomahome">
    <div class="tomahome-display">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-5">
                    <h1 class="hidden-sm">UNIVERSIDAD <strong>EAN</strong>, LA UNIVERSIDAD DE LOS <strong>EMPRENDEDORES</strong> </h1>
                    <div class="embed-responsive embed-responsive-16by9">

				<video id="movie" width="640" height="360" preload controls>
					<source src="{{ URL::to('/') }}/videos/PregradoPresencial.mov" />
				</video>

                    </div>
                </div>
                <div class="col-md-6 col-sm-7">
                    <form method="POST" action="{{ URL::route('landing.contact', $landing->name) }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-group title">
                            <h3 class="text-center text-uppercase"><strong>Carreras Profesionales Presenciales </strong> </h3>
                            <h4 class="text-center">INGRESE SUS DATOS Y RECIBA INFORMACIÓN PERSONALIZADA</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="name">Nombres</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Ej: Juan Camilo " data-required>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="lastname">Apellidos</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Ej: Suarez Hernandez" data-required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone">Celular o Teléfono</label>
                            <input type="phone" class="form-control" id="phone" name="phone" placeholder="Ej: (1) 7943012" data-required>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Ej: juancamilo@gmail.com" data-required>
                        </div>
                        <div class="form-group">
                            <label for="program">Programa de interes</label>
                            <select class="form-control" id="program" name="program" required="required" data-required>

				<option value="">Seleccione..</option>
				<option value="Administración de Empresas">Administración de Empresas</option>
				<option value="Economía">Economía</option>
				<option value="Estudios y Gestión Cultural">Estudios y Gestión Cultural</option>
				<option value="Ingeniería Ambiental">Ingeniería Ambiental</option>
				<option value="Ingeniería en Energías">Ingeniería en Energías</option>
				<option value="Ingeniería de Producción">Ingeniería de Producción</option>
				<option value="Ingeniería Química">Ingeniería Química</option>
				<option value="Ingeniería de Sistemas">Ingeniería de Sistemas</option>
				<option value="Lenguas Modernas">Lenguas Modernas</option>
				<option value="Negocios Internacionales">Negocios Internacionales</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="terms" data-required> Acepto <a href="http://ean.edu.co/documents/acuerdo005de2014.pdf" target="blank">Términos y Condiciones</a>.
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning btn-lg ">Envíar formulario</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content2">
    <div class="container">
        <h3 class="text-center text-uppercase">¿POR QUÉ ESTUDIAR una Carrera Profesional en metodología presencial  EN LA UNIVERSIDAD EAN?</h3>
    </div>
</div>
<div class="content3">
    <div class="container">
        <div class="row nuper-bottom wow bounceInLeft">
            <div class="col-md-1 col-sm-1">
                <span class="nuper">1</span>
            </div>
            <div class="col-md-11 col-sm-11">
                <span class="lead">Por ser una Universidad que imparte <strong>programas enfocados a la formación emprendedora sostenible</strong>, desarrollando conocimiento útil y capacidades para incrementar la competitividad del país.</span>
            </div>
        </div>
        <div class="row nuper-bottom wow bounceInLeft" data-wow-delay="0.3s">
            <div class="col-md-1 col-sm-1">
                <span class="nuper">2</span>
            </div>
            <div class="col-md-11 col-sm-11">
                <span class="lead">Por tener <strong> Acreditación Institucional de Alta Calidad </strong> otorgada por el Ministerio de Educación Nacional.</span>
            </div>
        </div>
        <div class="row nuper-bottom wow bounceInLeft" data-wow-delay="0.3s">
            <div class="col-md-1 col-sm-1">
                <span class="nuper">3</span>
            </div>
            <div class="col-md-11 col-sm-11">
                <span class="lead">Por contar con un <strong> Sistema de Gestión de Calidad </strong> y una certificación para todos sus procesos que garantiza a sus estudiantes la calidad de sus servicios.</span>
            </div>
        </div>
        <div class="row nuper-bottom wow bounceInLeft" data-wow-delay="0.3s">
            <div class="col-md-1 col-sm-1">
                <span class="nuper">4</span>
            </div>
            <div class="col-md-11 col-sm-11">
                <span class="lead">Por las <strong>opciones de movilidad internacional</strong>  con universidades en Europa, Asía, Norte, Centro y Suramérica.</span>
            </div>
        </div>
        <div class="row nuper-bottom wow bounceInLeft" data-wow-delay="0.3s">
            <div class="col-md-1 col-sm-1">
                <span class="nuper">5</span>
            </div>
            <div class="col-md-11 col-sm-11">
                <span class="lead">Por el <strong> perfil internacional del claustro docente</strong> de la Universidad EAN.</span>
            </div>
        </div>
    </div>
</div>

<!-- JS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdn.ulcommerce.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
<script src="//fast.wistia.net/assets/external/E-v1.js" async></script>
<script src="//cdn.ulcommerce.com/WOW/1.0.2/js/wow.min.js" type="text/javascript"></script>
<script type="text/javascript">
    new WOW().init();
</script>
<script type="text/javascript" src="/js/custom.js"></script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MNCKFS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MNCKFS');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1650524101903418');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1650524101903418&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</body>
</html>
