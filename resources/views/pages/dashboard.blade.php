@extends('app')
@section('title', $title)
@section('content')
    <div class="container">

        @include('layouts.nav')

        <div class="jumbotron">
            <h3 class="page-header">Exportar contactos</h3>

            <form action="export" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @forelse($landings as $key => $landing)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="landing[{{$key}}]" value="{{ $landing->id }}" checked>
                            {{ $landing->title }}
                        </label>
                    </div>
                @empty
                @endforelse

                <hr/>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="date" name="start_date" class="form-control" value="{{ date('Y-m-d') }}">
                        </div>
                        <div class="col-md-6">
                            <input type="date" name="end_date" class="form-control" value="{{ date('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success btn-block">Exportar</button>
            </form>

        </div>

    </div>
@stop
