<!DOCTYPE html>
<html lang="es-co">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-TileColor" content="#b2ba2c">
    <meta name="msapplication-TileImage" content="//latam.cdn.ulcommerce.com/ean/favicons/mstile-144x144.png">
    <meta name="msapplication-config" content="//latam.cdn.ulcommerce.com/ean/favicons/browserconfig.xml">
    <meta name="theme-color" content="#b2ba2c">

    <title>Gracias - EAN  </title>
    <meta name="description" content="">
    <link rel="canonical" href="http://landing.ean.edu.co/doctorados"/>
    <meta name="robots" content="index,follow">

    <link rel="apple-touch-icon" sizes="57x57" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="//latam.cdn.ulcommerce.com/ean/favicons/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="//latam.cdn.ulcommerce.com/ean/favicons/manifest.json">
    <link rel="shortcut icon" href="//latam.cdn.ulcommerce.com/ean/favicons/favicon.ico">

    <!-- CSS -->
    <link href="//cdn.ulcommerce.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link href="//cdn.ulcommerce.com/WOW/1.0.2/css/animate.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1650524101903418');
        fbq('track', "PageView");
        fbq('track', 'Lead');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1650524101903418&ev=PageView&noscript=1"
                /></noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>

<div class="content4 content3">
    <div class="container">
        <h1 class="text-center text-uppercase wow bounceInLeft">
            Enviado con éxito
        </h1>
        <h5 class="text-center">
            <a href="{{ URL::route('landing.index', Session::get('landing')) }}" class="wow bounceInLeft">Regresar</a>
        </h5>
    </div>
</div>

<!-- JS -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//cdn.ulcommerce.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
<script src="//cdn.ulcommerce.com/WOW/1.0.2/js/wow.min.js" type="text/javascript"></script>
<script type="text/javascript">
    new WOW().init();
</script>
<script type="text/javascript" src="/js/custom.js"></script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MNCKFS"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MNCKFS');</script>
<!-- End Google Tag Manager -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','//connect.facebook.net/en_US/fbevents.js');

fbq('init', '1650524101903418');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1650524101903418&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">twttr.conversion.trackPid('ntsrd', { tw_sale_amount: 0, tw_order_quantity: 0 });</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=ntsrd&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=ntsrd&p_id=Twitter&tw_sale_amount=0&tw_order_quantity=0" />
</noscript>
</body>
</html>