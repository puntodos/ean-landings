@if($errors->any() or Session::has('errors'))
    <div class="row nuper-bottom wow bounceInLeft text-danger">
        <div class="col-md-12">
            @foreach ($errors->all(':message') as $message)
                {{ $message }}
            @endforeach
        </div>
    </div>
@endif

@if(Session::has('message'))
    <div class="row nuper-bottom wow bounceInLeft">
        <div class="col-md-12">
            {{ Session::get('message') }}
        </div>
    </div>
@endif