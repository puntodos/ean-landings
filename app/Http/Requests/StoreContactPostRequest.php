<?php

namespace Ean\Http\Requests;

use Ean\Http\Requests\Request;

class StoreContactPostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'terms' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Todos los campos son requeridos.',
            'email.required' => 'Todos los campos son requeridos.',
        ];
    }
}
