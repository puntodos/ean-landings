<?php

namespace Ean\Http\Requests;

use Ean\Http\Requests\Request;

class UpdateLandingPutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:landings,name,'.$this->landing,
            'title' => 'required',
            'view' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es requerido',
            'name.unique' => 'El nombre es único',
            'title.required' => 'El titulo es requerido',
            'view.required' => 'La vista es requerida',
        ];
    }
}
