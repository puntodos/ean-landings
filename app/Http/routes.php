<?php

// Authentication routes...

Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', ['as' => 'login.store', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['middleware' => 'auth', 'as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Dashboard
Route::get('/dashboard', ['middleware' => 'auth', 'as' => 'login', 'uses' => 'AdminController@dashboard']);
Route::get('/{landing}', ['as' => 'landing.index', 'uses' => 'PageController@home']);
Route::post('/landing/{landing}', ['as' => 'landing.contact', 'uses' => 'PageController@contact']);
Route::post('/export', ['middleware' => 'auth', 'as' => 'export', 'uses' => 'AdminController@export']);

Route::get('/{landing}/send', ['as' => 'landing.send', 'uses' => 'PageController@send']);

// Landings
Route::group(['middleware' => 'auth'], function() {
    Route::resource('/dashboard/landing', 'LandingController');
});

// Missing
Route::get('/', 'PageController@any');
Route::get('{any}', 'PageController@any');
