<?php

namespace Ean\Http\Controllers;

use Ean\Http\Requests\StoreLandingPostRequest;
use Ean\Http\Requests\UpdateLandingPutRequest;
use Ean\Landing;
use Illuminate\Http\Request;

use Ean\Http\Requests;
use Ean\Http\Controllers\Controller;

class LandingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data['title'] = 'Landings';
        $data['landings'] = Landing::all();
        return view('landings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $data['title'] = 'Crear Landing';
        return view('landings.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreLandingPostRequest $request
     * @return Response
     */
    public function store(StoreLandingPostRequest $request)
    {
        $inputs = $request->only('name', 'title', 'view');
        Landing::create($inputs);
        return redirect()->route('dashboard.landing.index')->withMessage('Se creo nuevo landing');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['landing'] = Landing::find($id);
        $data['title'] = $data['landing']->title;
        return view('landings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateLandingPutRequest $request
     * @param  int $id
     * @return Response
     */
    public function update(UpdateLandingPutRequest $request, $id)
    {
        $inputs = $request->only('name', 'title', 'view');
        Landing::updateOrCreate(['id' => $id], $inputs);
        return redirect()->route('dashboard.landing.index')->withMessage('Se actualizó el landing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $banner = Landing::find($id);
        $banner->delete();

        return redirect()->route('landing.index')->withMessage('Se eliminó el landing');
    }


}
