<?php

namespace Ean\Http\Controllers;

use Ean\Contact;
use Ean\Landing;
use Illuminate\Http\Request;

use Ean\Http\Requests;
use Ean\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller
{

    private $dataExcel = NULL;

    /**
     * Dashboard
     *
     * @return Response
     */
    public function dashboard()
    {
        $data['title'] = 'Dashboard';
        $data['landings'] = Landing::all();
        return view('pages.dashboard', $data);
    }

    /**
     * Export excel
     *
     * @param $request
     * @return Response
     */
    public function export(Request $request)
    {
        $date[0] = $request->has('start_date') ? $request->get('start_date').' 00:00:00' : '1990-01-01 00:00:00';
        $date[1] = $request->has('end_date') ? $request->get('end_date').' 23:59:00' : date('Y-m-d H:i:s');
        $contacts = Contact::whereBetween('created_at', $date)->whereIn('landing_id', $request->get('landing'))->get();

        $headers = array('ID', 'Nombres', 'Apellidos', 'Email', 'Teléfono', 'Programa', 'Landing', 'Fecha', 'Hora');

        $this->dataExcel[] = $headers;

        foreach ($contacts as $c) {
            $this->dataExcel[] = [
                $c->id,
                $c->name,
                $c->lastname,
                $c->email,
                $c->phone,
                $c->program,
                $c->landing->title,
                explode(' ', $c->created_at)[0],
                explode(' ', $c->created_at)[1],
            ];
        }


        Excel::create('contactos-'.date('d/m/Y'), function($excel) {
            $excel->sheet('Contactos', function($sheet) {

                $sheet->cells('A1:Q1', function($cells) {
                    $cells->setFontColor('#000000');
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                });

                $sheet->fromArray($this->dataExcel, null, 'A1', false, false);

            });
        })->download('xlsx');

    }

}
