<?php

namespace Ean\Http\Controllers;

use Ean\Contact;
use Ean\Landing;
use Illuminate\Http\Request;

use Ean\Http\Requests;
use Ean\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{

    /**
     * @param $landing
     * @return \Illuminate\View\View
     */
    public function home($landing)
    {
        $landing = Landing::whereName($landing)->firstOrFail();
        return view('pages.'.$landing->view, ['landing' => $landing]);
    }

    /**
     * Enviado con exito
     * @return \Illuminate\View\View
     */
    public function send()
    {
        return view('pages.send');
    }

    /**
     * @param Requests\StoreContactPostRequest $request
     * @param $landing
     */
    public function contact(Requests\StoreContactPostRequest $request, $landing)
    {
        $landing = Landing::whereName($landing)->firstOrFail();
        $inputs = $request->only('name', 'lastname', 'email', 'program', 'terms', 'phone');
        $inputs['landing_id'] = $landing->id;
        $contact = Contact::create($inputs);

        $message = $contact->toArray();
        $message['landing'] = $landing->title;

        Mail::send('emails.contact', $message, function($message) use ($landing)
        {
            $message->to('mailsipgco@gmail.com', 'Ean')->cc('informacion@ean.edu.co', 'Ean')->subject('Nuevo contacto - '.$landing->title);
        });

        return redirect()->route('landing.send', $landing->name)->withLanding($landing->name);

    }

    public function any()
    {
        //return redirect()->to('http://www.ean.edu.co/');
	//return redirect()->to('http://104.236.95.98/');
    }

}
