<?php

namespace Ean;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $fillable = ['name', 'lastname', 'landing_id', 'email', 'program', 'terms', 'phone'];


    public function landing()
    {
        return $this->belongsTo('Ean\Landing');
    }

}
