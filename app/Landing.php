<?php

namespace Ean;

use Illuminate\Database\Eloquent\Model;

class Landing extends Model {

    protected $fillable = ['name', 'title', 'view'];

}
