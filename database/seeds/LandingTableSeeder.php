<?php

use Ean\Landing;
use Illuminate\Database\Seeder;

class LandingTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            ['name' => 'landing1', 'title' => 'Landing 1', 'view' => 'landing1']
        ];

        foreach ($data as $value) {
            Landing::create($value);
        }
    }
}