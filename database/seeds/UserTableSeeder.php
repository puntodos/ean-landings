<?php

use Ean\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            ['name' => 'Diego', 'email' => 'diego@puntodos.co', 'password' => Hash::make('diego123')]
        ];

        foreach ($data as $value) {
            User::create($value);
        }
    }
}
