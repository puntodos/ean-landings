(function($) {

    $(document).ready(function() {
        $('form').submit(function(e) {
            $(this).find('[data-required]').each(function() {
                if($(this).val() == '') {
                    $(this).closest('.form-group').addClass('has-error has-feedback');
                    e.preventDefault();
                    return false;
                }
            })
            if(!$('[name="terms"]').prop('checked')) {
                $('[name="terms"]').closest('.form-group').addClass('has-error has-feedback');
                e.preventDefault();
                return false;
            }
        })
    })

})(jQuery)